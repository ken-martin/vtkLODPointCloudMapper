vtk_module(vtkLODPointCloudMapper
  BACKEND
    OpenGL2
  DEPENDS
    vtkCommonCore
    vtkFiltersPoints
    vtkRendering${VTK_RENDERING_BACKEND}
  PRIVATE_DEPENDS
    vtkglew
  TEST_DEPENDS
    vtkTestingCore
    vtkTestingRendering
    vtkInteractionStyle
  KIT
    vtkRemote
)
