/*=========================================================================

  Program:   Visualization Toolkit

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkLODPointCloudMapper - draw PointClouds with LOD
// .SECTION Description
// An OpenGL mapper that adaptively renders multiblock point clouds

// This mapper only implements a very small subset of VTK's features.
// Consider it experimental and as a starting point for other uses. This
// mapper is designed to take a multiblock point cloud of the form
// typically produced by vtkHierachicalBinningFilter and run through
// vtkPointCloudToMultiBlock followed by vtkMultiBlockToCompressed. If
// the compressed stage is skipped it will be done by the mapper
// internally.
//
// The result of these filters should be a multiblockdataset where the
// first block contains a sampling of the points from the entire point
// cloud.  Additional blocks will contain smaller subdivisions. A common
// arrangement is 1 grandparent + 8 parents + 64 children in an octree
// arrangement for a total of 73 blocks.
//
// This mapper uses unsigned shorts to store the XYZ values and uses
// either 3 unsigned chars for color or one unsigned short for single
// valued scalars.  When using single valued scalars the lookup table is
// hardcoded. When there is no color or scalars a blue to yellow pseudo
// coloring is done based on depth.
//
// This mapper uses timing and coverage metrics to determine what blocks
// to draw  and how much of any given block to draw to maintain the
// desired render rate. On a laptop with a nvidia 2Gig 960M (midrange
// card) I see around one billoion points/sec for a dataset of 140
//  million points with RGB color
//
// While this mapper can be fed through a normal VTK pipeline, it also
// supports  being fed data one piece at a time using the SetDataForPiece
// method. The intent is that the file IO can be handled in one thread, a
// piece at a time, while the rendering  is handled in another thread.
// SetDataForPiece is what you would use to  pass data from the file IO
// thread to the rendering thread.

#ifndef vtkLODPointCloudMapper_h
#define vtkLODPointCloudMapper_h

#include "vtkPolyDataMapper.h"
#include <vector> // used for ivars
#include "vtkLODPointCloudMapperModule.h" // For export macro

class vtkLODPointCloudMapperPiece;
class vtkShaderProgram;
class vtkOpenGLRenderWindow;
class vtkPoints;
class vtkUnsignedCharArray;

class VTKLODPOINTCLOUDMAPPER_EXPORT vtkLODPointCloudMapper : public vtkPolyDataMapper
{
public:
  static vtkLODPointCloudMapper* New();
  vtkTypeMacro(vtkLODPointCloudMapper, vtkPolyDataMapper)
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Add a piece of the point cloud, typically passed from a file IO thread
  void SetDataForPiece(int piece, vtkPolyData *);
  void ClearPieces();
  vtkPolyData *GetDataForPiece(int i);
  int GetNumberOfPieces() {
    return static_cast<int>(this->Pieces.size()); }

  // Description:
  // Release any graphics resources that are being consumed by this mapper.
  // The parameter window could be used to determine which graphic
  // resources to release.
  void ReleaseGraphicsResources(vtkWindow *);

  // Description:
  // Get the most recent rendering rate in points/sec
  float GetLastPointRate() { return this->LastPointRate; }

  // Description:
  // Return bounding box (array of six doubles) of data expressed as
  // (xmin,xmax, ymin,ymax, zmin,zmax).
  virtual double *GetBounds();
  virtual void GetBounds(double bounds[6])
    { this->vtkAbstractMapper3D::GetBounds(bounds); }

  // Description:
  // set new scalar array for coloring the points
  void SetNewColorData(const char *);
  void ClearColorData();

protected:
  vtkLODPointCloudMapper();
  ~vtkLODPointCloudMapper();

  int FillInputPortInformation(int port, vtkInformation* info);

  // the main entry point
  virtual void Render(vtkRenderer *ren, vtkActor *act);
  virtual void RenderPiece(vtkRenderer *, vtkActor *) {};

  // used to build the input vector from a traditional
  // pipeline
  void BuildVectorFromInput();
  vtkTimeStamp VectorBuildTime;

  float ComputeCoverage(vtkRenderer *ren, vtkActor *act);

  void UpdateShaders(vtkOpenGLRenderWindow *);

  // render one piece of the data
  void RenderPointCloudPiece(
    vtkRenderer *ren,
    vtkActor *act,
    vtkLODPointCloudMapperPiece *piece,
  	size_t pointsToRender,
    vtkMatrix4x4 *wcdc);

  // The VBO and its layout.
  std::vector<vtkLODPointCloudMapperPiece *> Pieces;

  // the shader program we use to render these
  vtkShaderProgram *ShaderProgram;

  unsigned int TimerQuery;
  float LastTimedPoints;
  float LastPointRate; // points/sec

  // used for matrix ops
  vtkMatrix4x4 *TempMatrix4;
  vtkMatrix4x4 *MCDCMatrix;
  vtkTransform *TempTransform;
  vtkTimeStamp ShaderBuildTime;
  int LastColorComponents;
  int ColorComponents;

private:
  vtkLODPointCloudMapper(const vtkLODPointCloudMapper&); // Not implemented.
  void operator=(const vtkLODPointCloudMapper&); // Not implemented.
};

#endif
