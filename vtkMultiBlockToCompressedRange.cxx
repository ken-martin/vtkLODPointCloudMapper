/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkMultiBlockToCompressedRange.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkMultiBlockToCompressedRange.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkDoubleArray.h"
#include "vtkDataObjectTreeIterator.h"
#include "vtkUnsignedShortArray.h"


vtkStandardNewMacro(vtkMultiBlockToCompressedRange);

//-----------------------------------------------------------------------------
vtkMultiBlockToCompressedRange::vtkMultiBlockToCompressedRange()
{
}

//-----------------------------------------------------------------------------
int vtkMultiBlockToCompressedRange::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkMultiBlockDataSet *input = vtkMultiBlockDataSet::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  if (!input)
    {
    return 0;
    }

  vtkInformation* info = outputVector->GetInformationObject(0);
  vtkMultiBlockDataSet *output = vtkMultiBlockDataSet::SafeDownCast(
    info->Get(vtkDataObject::DATA_OBJECT()));
  if (!output)
    {
    return 0;
    }

  unsigned int numBlocks = input->GetNumberOfBlocks();
  output->SetNumberOfBlocks(numBlocks);

  vtkDataObjectTreeIterator* iter = input->NewTreeIterator();
  iter->TraverseSubTreeOff();
  iter->VisitOnlyLeavesOff();

  int maxCount = 0;
  for (iter->InitTraversal(); !iter->IsDoneWithTraversal();
    iter->GoToNextItem())
    {
    maxCount++;
    }

  int blockIdx = 0;
  for (iter->InitTraversal();
       !iter->IsDoneWithTraversal() && !this->AbortExecute;
       iter->GoToNextItem(), blockIdx++)
    {
    vtkDataObject* dObj = iter->GetCurrentDataObject();
    if (dObj)
      {
      vtkDataObject* block = this->Compress(dObj);
      if (block)
        {
        output->SetDataSet(iter, block);
        block->Delete();
        }
      }
    this->UpdateProgress(static_cast<float>(blockIdx)/maxCount);
    }

  iter->Delete();
  return 1;
}

namespace
{

template< typename PointDataType >
void vtkMultiBlockToCompressedRangeConvertPointsTemplate(
    unsigned short *uspts,
    PointDataType* points,
    vtkIdType numPts,
    vtkIdType numComponents,
    double *origin,
    double *scale)
{
  for (int i = 0; i < numPts; i++)
    {
    for (int j = 0; j < numComponents; j++)
      {
      uspts[i*numComponents+j] = static_cast<unsigned short>
        (65535.0*(points[j] - origin[j])/scale[j]);
      }
    points += numComponents;
    }
}

}

//----------------------------------------------------------------------------
vtkDataObject* vtkMultiBlockToCompressedRange::Compress(
  vtkDataObject* input)
{
  vtkDataObject* output = 0;
  if (input->IsA("vtkCompositeDataSet"))
    {
    vtkCompositeDataSet* mbInput = vtkCompositeDataSet::SafeDownCast(input);

    output = input->NewInstance();
    vtkCompositeDataSet* mbOutput =
      vtkCompositeDataSet::SafeDownCast(output);
    mbOutput->CopyStructure(mbInput);

    vtkCompositeDataIterator* inIter = mbInput->NewIterator();
    for (inIter->InitTraversal(); !inIter->IsDoneWithTraversal();
      inIter->GoToNextItem())
      {
      vtkDataObject* src = inIter->GetCurrentDataObject();
      vtkDataObject* dest = 0;
      if (src)
        {
        dest = vtkMultiBlockToCompressedRange::Compress(src);
        }
      mbOutput->SetDataSet(inIter, dest);
      }
    }
  else
    {
    vtkPointSet* ds = vtkPointSet::SafeDownCast(input);
    if (ds && ds->GetPoints())
      {
      output = ds->NewInstance();
      vtkPointSet *outputPS = vtkPointSet::SafeDownCast(output);
      vtkFieldData *outFD = output->GetFieldData();

      // compress the points
      vtkPoints *pts = ds->GetPoints();
      double *bounds = pts->GetBounds();
      double origin[3];
      double scale[3];
      for (int i = 0; i < 3; i++)
        {
        origin[i] = bounds[i*2];
        scale[i] = bounds[i*2+1] - bounds[i*2];
        }
      vtkNew<vtkDoubleArray> factors;
      factors->SetName("PointCompressionFactors");
      factors->SetNumberOfComponents(3);
      outFD->AddArray(factors.Get());
      factors->InsertTuple3(0,origin[0],origin[1],origin[2]);
      factors->InsertTuple3(1,scale[0],scale[1],scale[2]);

      vtkIdType numPts = pts->GetNumberOfPoints();
      vtkNew<vtkPoints> outPts;
      outPts->SetDataTypeToUnsignedShort();
      outPts->SetNumberOfPoints(numPts);
      unsigned short *uspts =
        static_cast<unsigned short *>(outPts->GetVoidPointer(0));
      switch(pts->GetDataType())
        {
        vtkTemplateMacro(
          vtkMultiBlockToCompressedRangeConvertPointsTemplate(
            uspts,
            static_cast<VTK_TT*>(pts->GetVoidPointer(0)),
            numPts,
            3,
            origin, scale));
        }
      outputPS->SetPoints(outPts.Get());

      // convert the pointdata
      vtkPointData *outPD = outputPS->GetPointData();
      vtkNew<vtkDoubleArray> pdfactors;
      pdfactors->SetName("PointDataCompressionFactors");
      pdfactors->SetNumberOfComponents(1);
      outFD->AddArray(pdfactors.Get());
      int pdCount = 0;

      vtkPointData *inPD = ds->GetPointData();
      int numArrays = inPD->GetNumberOfArrays();
      for (int i = 0; i < numArrays; i++)
        {
        vtkDataArray *da = inPD->GetArray(i);
        // just pass through 8 and 16 bit data unchanged
        if (da->GetDataTypeSize() <= 2)
          {
          outPD->AddArray(da);
          for (int j = 0; j < da->GetNumberOfComponents(); j++)
            {
            pdfactors->InsertTuple1(pdCount,0.0);
            pdfactors->InsertTuple1(pdCount+1,1.0);
            pdCount += 2;
            }
          }
        else
          {
          // convert to unsigned short and scale the data
          vtkNew<vtkUnsignedShortArray> usa;
          int numComponents = da->GetNumberOfComponents();
          usa->SetNumberOfComponents(numComponents);
          usa->SetNumberOfTuples(da->GetNumberOfTuples());
          usa->SetName(da->GetName());

          double *origin = new double [numComponents];
          double *scale = new double [numComponents];
          for (int i = 0; i < numComponents; i++)
            {
            double range[2];
            da->GetRange(range,i);
            origin[i] = range[0];
            scale[i] = range[1] - range[0];
            }
          unsigned short *uspts =
            static_cast<unsigned short *>(usa->GetVoidPointer(0));
          switch(da->GetDataType())
            {
            vtkTemplateMacro(
              vtkMultiBlockToCompressedRangeConvertPointsTemplate(
                uspts,
                static_cast<VTK_TT*>(da->GetVoidPointer(0)),
                da->GetNumberOfTuples(),
                numComponents,
                origin, scale));
            }
          for (int i = 0; i < numComponents; i++)
            {
            pdfactors->InsertTuple1(pdCount,origin[i]);
            pdfactors->InsertTuple1(pdCount+1,scale[i]);
            pdCount += 2;
            }
          outPD->AddArray(usa.Get());
          delete [] origin;
          delete [] scale;
          }
        }
       if (inPD->GetScalars())
        {
        outPD->SetActiveScalars(inPD->GetScalars()->GetName());
        }
      }
    }
  return output;
}


//-----------------------------------------------------------------------------
void vtkMultiBlockToCompressedRange::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
