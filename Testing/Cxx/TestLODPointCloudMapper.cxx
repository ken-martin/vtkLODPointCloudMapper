/*=========================================================================

  Program:   Visualization Toolkit

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// This test covers the depth of field post-processing render pass.
//
// The command line arguments are:
// -I        => run in interactive mode; unless this is used, the program will
//              not allow interaction and exit

#include "vtkTestUtilities.h"
#include "vtkRegressionTestImage.h"

#include "vtkNew.h"
#include "vtkProperty.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderWindow.h"
#include "vtkOpenGLRenderer.h"
#include "vtkActor.h"
#include "vtkPolyDataMapper.h"
#include "vtkCamera.h"
#include "vtkCameraPass.h"
#include "vtkHierarchicalBinningFilter.h"
#include "vtkPointCloudToMultiBlock.h"
#include "vtkLODPointCloudMapper.h"
#include "vtkMultiBlockToCompressedRange.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkMinimalStandardRandomSequence.h"
#include "vtkPointFillPass.h"
#include "vtkRenderStepsPass.h"
#include "vtkColorTransferFunction.h"
#include "vtkPointData.h"

#include "vtkEDLShading.h"


#include "vtkTimerLog.h"

// build synthetic dataset with color
vtkPolyData *BuildLand(double targetPoints)
{
  vtkNew<vtkMinimalStandardRandomSequence> randseq;
  randseq->SetSeedOnly(3);

  int size[2];
  size[0] = sqrt(targetPoints);
  size[1] = targetPoints/size[0];

  // to make life easy we generate somethign larger than what we need
  // and the just throw away the parts we do not need
  int psize[2];
  for (int i = 0; i < 2; i++)
    {
    psize[i] = 1;
    while (psize[i] + 1 < size[i])
      {
      psize[i] *= 2;
      }
    psize[i] += 1;
    }

  float *image = new float[psize[0]*psize[1]];
  for (int i = 0; i < psize[0]*psize[1]; i++)
    {
    image[i] = 0.0;
    }
  int xstep = psize[0] - 1;
  int ystep = psize[1] - 1;
  image[0] = randseq->GetValue() + 1.0; randseq->Next();
  image[xstep] = randseq->GetValue() + 1.0; randseq->Next();
  image[ystep*psize[0]] = randseq->GetValue() + 1.0; randseq->Next();
  image[xstep + ystep*psize[0]] = randseq->GetValue() + 1.0; randseq->Next();
  while (xstep > 1 || ystep > 1)
    {
    // split on X
    if (xstep > 1)
      {
      xstep /= 2;
      for (int j = 0; j < psize[1]; j += ystep)
        {
        int yoffset = j*psize[0];
        for (int i = xstep; i < psize[0]; i = i + xstep*2)
          {
          // is the pixel not set?
          if (image[i + j*psize[0]] == 0)
            {
            // get the average of the two
            image[i+yoffset] = (image[i-xstep + yoffset] + image[i+xstep + yoffset])*0.5
              + xstep*(randseq->GetValue() - 0.5)/psize[0]; randseq->Next();
            }
          }
        }
      }
    // split on Y
    if (ystep > 1)
      {
      ystep /= 2;
      for (int i = 0; i < psize[0]; i += xstep)
        {
        for (int j = ystep; j < psize[1]; j = j + ystep*2)
          {
          // is the pixel not set?
          if (image[i + j*psize[0]] == 0)
            {
            // get the average of the two
            image[i+j*psize[0]] = (image[i + (j-ystep)*psize[0]] + image[i + (j+ystep)*psize[0]])*0.5
              + ystep*(randseq->GetValue() - 0.5)/psize[1]; randseq->Next();
            }
          }
        }
      }
    }

  // copy the data to points and colors
  int numPoints = size[0]*size[1];
  vtkNew<vtkPoints> points;
  points->SetNumberOfPoints(numPoints);
  vtkPolyData *pd = vtkPolyData::New();
  pd->SetPoints(points.Get());
  vtkNew<vtkUnsignedCharArray> colors;
  colors->SetNumberOfComponents(3);
  colors->SetNumberOfTuples(numPoints);
  colors->SetName("colors");
  pd->GetPointData()->SetScalars(colors.Get());
  vtkNew<vtkColorTransferFunction> lut;
  lut->AddHSVPoint(0.7,0.65,0.7,0.4);
  lut->AddHSVPoint(0.89,0.6,0.6,0.7);
  lut->AddHSVPoint(0.9,0.2,0.3,0.8);
  lut->AddHSVPoint(0.91,0.4,0.9,0.6);
  lut->AddHSVPoint(1.1,0.4,0.8,0.4);
  lut->AddHSVPoint(1.3,0.2,0.5,0.5);
  lut->AddHSVPoint(1.4,0.0,0.0,1.0);
  lut->SetColorSpaceToHSV();

  for (int j = 0; j < size[1]; j++)
    {
    for (int i = 0; i < size[0]; i++)
      {
      float height = image[i+j*psize[0]];
      unsigned char *c = lut->MapValue(height);
      colors->SetTypedTuple(i+j*size[0],c);
      if (height < 0.9)
        {
        height = 0.9;
        }
      points->SetPoint(i+j*size[0],
        static_cast<float>(i)/size[0],
        static_cast<float>(j)/size[0],
        height);
      }
    }
  delete [] image;

  return pd;
}

int TestLODPointCloudMapper(int argc, char* argv[])
{
  vtkNew<vtkRenderWindowInteractor> iren;
  vtkNew<vtkRenderWindow> renWin;
  renWin->SetMultiSamples(0);
  iren->SetRenderWindow(renWin.Get());
  vtkNew<vtkRenderer> renderer;
  renWin->AddRenderer(renderer.Get());

  double targetPoints = 1.0e5;
  vtkPolyData *pd = BuildLand(targetPoints);

  // bin the data
  vtkNew<vtkHierarchicalBinningFilter> binner;
  binner->SetInputData(pd);
  pd->Delete();
  binner->AutomaticOff();
  binner->SetDivisions(2,2,2);
  binner->SetBounds(pd->GetBounds());
  binner->Update();

  // convert to multiblock
  vtkNew<vtkPointCloudToMultiBlock> cloudToMultiBlock;
  cloudToMultiBlock->SetInputConnection(binner->GetOutputPort());
  cloudToMultiBlock->Update();
  binner->GetOutput()->ReleaseData();

  vtkNew<vtkMultiBlockToCompressedRange> compress;
  compress->SetInputConnection(cloudToMultiBlock->GetOutputPort(0));
  compress->Update();
  cloudToMultiBlock->GetOutput()->ReleaseData();

  vtkNew<vtkLODPointCloudMapper> mapper;
  mapper->SetInputConnection(compress->GetOutputPort(0));
  vtkNew<vtkActor> actor;
  actor->SetMapper(mapper.Get());
  renderer->AddActor(actor.Get());

  renderer->SetNearClippingPlaneTolerance(0.1);
  renderer->SetClippingRangeExpansion(0.01);

  vtkOpenGLRenderer *glrenderer =
      vtkOpenGLRenderer::SafeDownCast(renderer.GetPointer());

  // create the basic VTK render steps
  vtkNew<vtkRenderStepsPass> basicPasses;

  // finally add the PF passs
  vtkNew<vtkCameraPass> camp;
  vtkNew<vtkPointFillPass> pfp;
  pfp->SetMinimumCandidateAngle(2.5);
  pfp->SetDelegatePass(basicPasses.Get());
  camp->SetDelegatePass(pfp.Get());

  // tell the renderer to use our point fill pass pipeline
  glrenderer->SetPass(camp.Get());

  // for fun up the number of points and
  // use EDL shading by uncommenting the
  // following
  // vtkNew<vtkEDLShading> edl;
  // edl->SetDelegatePass(camp.Get());
  // glrenderer->SetPass(edl.Get());

  renWin->SetSize(400,400);

  vtkNew<vtkTimerLog> timer;
  renderer->ResetCamera();
  renWin->Render();

  timer->StartTimer();
  int numRenders = 40;
  for (int i = 0; i < numRenders; ++i)
    {
    renderer->GetActiveCamera()->Azimuth(80.0/numRenders);
    renderer->GetActiveCamera()->Elevation(88.0/numRenders);
    renderer->ResetCameraClippingRange();
    renWin->Render();
    }
  timer->StopTimer();
  double elapsed = timer->GetElapsedTime();
  cerr << "loop render time: " << elapsed / numRenders << endl;
  cerr << "points per second: " <<  targetPoints*(numRenders/elapsed) << endl;

  renderer->GetActiveCamera()->SetPosition(0,0,1);
  renderer->GetActiveCamera()->SetFocalPoint(0,0,0);
  renderer->GetActiveCamera()->SetViewUp(0,1,0);
  renderer->ResetCamera();
  renderer->GetActiveCamera()->Elevation(-75.0);
  renderer->GetActiveCamera()->OrthogonalizeViewUp();
  renderer->GetActiveCamera()->Azimuth(30.0);
  renderer->ResetCameraClippingRange();
  renWin->Render();

  int retVal = vtkRegressionTestImage( renWin.Get() );

  if ( retVal == vtkRegressionTester::DO_INTERACTOR)
    {
    iren->SetDesiredUpdateRate(30.0);
    iren->SetStillUpdateRate(1.0);
    iren->Start();
    }
  return !retVal;
}
