/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkMultiBlockToCompressedRange.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkMultiBlockToCompressedRange - converts to compressed multiblock
// .SECTION Description
// This filter takes a multiblockdataset and compresses the data
// to be more suitable for quick point cloud rendering.  It compresses
// point coordinates and scalars to unsigned short

#ifndef vtkMultiBlockToCompressedRange_h
#define vtkMultiBlockToCompressedRange_h

#include "vtkMultiBlockDataSetAlgorithm.h"
#include "vtkLODPointCloudMapperModule.h" // For export macro

class VTKLODPOINTCLOUDMAPPER_EXPORT vtkMultiBlockToCompressedRange :
  public vtkMultiBlockDataSetAlgorithm
{
public:
  // Description:
  // Standard methods for instantiation, printing, and type information.
  static vtkMultiBlockToCompressedRange *New();
  vtkTypeMacro(vtkMultiBlockToCompressedRange, vtkMultiBlockDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Apply the compression on a vtkDataObject
  // returning a new vtkDataObject
  static vtkDataObject* Compress(vtkDataObject* input);

protected:
  vtkMultiBlockToCompressedRange();
  ~vtkMultiBlockToCompressedRange() {}

  // Usual data generation method
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

private:
  vtkMultiBlockToCompressedRange(const vtkMultiBlockToCompressedRange&);  // Not implemented.
  void operator=(const vtkMultiBlockToCompressedRange&);  // Not implemented.
};

#endif
