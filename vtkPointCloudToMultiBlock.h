/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPointCloudToMultiBlock.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkPointCloudToMultiBlock - Convert a hierarchical point cloud to multiblock
// .SECTION Description
// This filter takes the output of a vtkHierarchicalBinningFilter and
// converts the bins into a multiblock dataset

#ifndef vtkPointCloudToMultiBlock_h
#define vtkPointCloudToMultiBlock_h

#include "vtkMultiBlockDataSetAlgorithm.h"
#include "vtkLODPointCloudMapperModule.h" // For export macro

class VTKLODPOINTCLOUDMAPPER_EXPORT vtkPointCloudToMultiBlock :
  public vtkMultiBlockDataSetAlgorithm
{
public:
  // Description:
  // Standard methods for instantiation, printing, and type information.
  static vtkPointCloudToMultiBlock *New();
  vtkTypeMacro(vtkPointCloudToMultiBlock, vtkMultiBlockDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Turn on or off modulo sampling of the points. By default this is on and the
  // points in a given piece will be reordered in an attempt to reduce spatial
  // coherency.
  vtkSetMacro(ModuloOrdering,bool);
  vtkGetMacro(ModuloOrdering,bool);
  vtkBooleanMacro(ModuloOrdering,bool);

protected:
  vtkPointCloudToMultiBlock();
  ~vtkPointCloudToMultiBlock() {}

  // Usual data generation method
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  int FillInputPortInformation(int, vtkInformation *info);

  bool ModuloOrdering;

private:
  vtkPointCloudToMultiBlock(const vtkPointCloudToMultiBlock&);  // Not implemented.
  void operator=(const vtkPointCloudToMultiBlock&);  // Not implemented.
};

#endif
