/*=========================================================================

  Program:   Visualization Toolkit

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// tuesday 1:15

#include "vtkLODPointCloudMapper.h"

#include "vtkOpenGLHelper.h"

#include "vtkCommand.h"
#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkOpenGLActor.h"
#include "vtkOpenGLVertexArrayObject.h"
#include "vtkOpenGLVertexBufferObject.h"
#include "vtkPolyData.h"
#include "vtkOpenGLRenderer.h"
#include "vtkShaderProgram.h"
#include "vtkMatrix4x4.h"
#include "vtkOpenGLRenderWindow.h"
#include "vtkOpenGLShaderCache.h"
#include "vtkOpenGLCamera.h"
#include "vtkPointData.h"
#include "vtkScalarsToColors.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkDataObjectTreeIterator.h"
#include "vtkMultiBlockToCompressedRange.h"

#include "vtkTransform.h"

#include "vtk_glew.h"

class vtkLODPointCloudMapperPiece
{
public:
  void SetInput(vtkPolyData *pd)
    {
    if (this->Input == pd) { return; }
    if (this->Input) { this->Input->Delete(); }
    this->Input = pd;
    this->Input->Register(NULL);
    }
  vtkPolyData *Input;
  float ScreenCoverage;
  float VolumeCoverageRatio;
  float CoverageMetric;
  int Piece;
  int Level;

  // points
  vtkOpenGLVertexBufferObject *Points;
  vtkTimeStamp PointsVBOBuildTime;
  double Origin[3];
  double Scale[3];
  double Bounds[6];
  int VertexCount;

  // scalars
  vtkOpenGLVertexBufferObject *Colors;
  vtkTimeStamp ColorsVBOBuildTime;
  float ScalarScale;
  float ScalarOffset;

  vtkOpenGLVertexArrayObject *VAO;
  vtkTimeStamp VAOBuildTime;

  vtkLODPointCloudMapperPiece()
  {
    this->VAO = vtkOpenGLVertexArrayObject::New();
    this->Points = vtkOpenGLVertexBufferObject::New();
    this->Colors = vtkOpenGLVertexBufferObject::New();
    this->Input = NULL;
  }
  ~vtkLODPointCloudMapperPiece()
    {
    if (this->Input) { this->Input->Delete(); }
    this->Points->Delete();
    this->Colors->Delete();
    this->VAO->Delete();
    }
  void ReleaseGraphicsResources(vtkWindow *)
    {
    this->VAO->ReleaseGraphicsResources();
    this->Points->ReleaseGraphicsResources();
    this->Colors->ReleaseGraphicsResources();
    }
};


//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkLODPointCloudMapper)

//-----------------------------------------------------------------------------
vtkLODPointCloudMapper::vtkLODPointCloudMapper()
{
  this->ShaderProgram = 0;
  this->TempMatrix4 = vtkMatrix4x4::New();
  this->MCDCMatrix = vtkMatrix4x4::New();
  this->Pieces.reserve(73);
  this->LastTimedPoints = 1.0;
  this->TimerQuery = 0;
  this->LastPointRate = 30.0;
  this->TempTransform = vtkTransform::New();
  this->LastColorComponents = -1;
  this->ColorComponents = 0;
}

vtkLODPointCloudMapper::~vtkLODPointCloudMapper()
{
  this->ClearColorData();
  this->TempMatrix4->Delete();
  this->TempMatrix4 = NULL;
  this->MCDCMatrix->Delete();
  this->MCDCMatrix = NULL;
  this->TempTransform->Delete();
  this->TempTransform = NULL;
  for (int i = 0; i < this->Pieces.size(); i++)
    {
    if (this->Pieces[i])
      {
      delete this->Pieces[i];
      this->Pieces[i] = 0;
      }
    }
}

//----------------------------------------------------------------------------
int vtkLODPointCloudMapper::FillInputPortInformation(
  int vtkNotUsed(port), vtkInformation* info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkMultiBlockDataSet");
  return 1;
}

// Get the bounds for the input of this mapper as
// (Xmin,Xmax,Ymin,Ymax,Zmin,Zmax).
double *vtkLODPointCloudMapper::GetBounds()
{
  vtkCompositeDataSet *input = vtkCompositeDataSet::SafeDownCast(
    this->GetInputDataObject(0, 0));

  // if the input is more recent than our processed vector
  // then rebuild our vector
  if (this->Pieces.size() < 1 ||
      (input && input->GetMTime() > this->VectorBuildTime))
    {
    this->BuildVectorFromInput();
    }

  if (this->Pieces.size() == 0)
    {
    vtkMath::UninitializeBounds(this->Bounds);
    }
  else
    {
    for (int i = 0; i < 6; i++)
      {
      this->Bounds[i] = this->Pieces[0]->Bounds[i];
      }
    }
  return this->Bounds;
}

void vtkLODPointCloudMapper::ClearPieces()
{
  this->ClearColorData();
  for (int i = 0; i < this->Pieces.size(); i++)
    {
    if (this->Pieces[i])
      {
      this->Pieces[i]->ReleaseGraphicsResources(NULL);
      delete this->Pieces[i];
      this->Pieces[i] = 0;
      }
    }
  this->Pieces.resize(0);
}

vtkPolyData *vtkLODPointCloudMapper::GetDataForPiece(int pieceNum)
{
  if (this->Pieces.size() < pieceNum)
    {
    return NULL;
    }
  return this->Pieces[pieceNum]->Input;
}

void vtkLODPointCloudMapper::SetDataForPiece(
  int pieceNum,
  vtkPolyData *pd)
{
  if (pieceNum >= this->Pieces.size())
    {
    this->Pieces.resize(pieceNum+1);
    }
  if (!this->Pieces[pieceNum])
    {
    this->Pieces[pieceNum] = new vtkLODPointCloudMapperPiece;
    }

  this->Pieces[pieceNum]->SetInput(pd);
  this->Pieces[pieceNum]->Piece = pieceNum;
  this->Pieces[pieceNum]->Level = 1 + (pieceNum > 0 ? 1 : 0) + (pieceNum > 8 ? 1 : 0) + (pieceNum > 72 ? 1 : 0);
  this->Pieces[pieceNum]->VertexCount = 0;

  if (!pd)
    {
    return;
    }

  vtkDataArray *cda = pd->GetFieldData()->GetArray("PointCompressionFactors");
  if (!cda)
    {
    vtkErrorMacro("Mapper input is missing required fields");
    return;
    }

  this->Pieces[pieceNum]->VertexCount = pd->GetNumberOfPoints();

  vtkDataArray *scalars = pd->GetPointData()->GetScalars();
  if (pieceNum == 0)
    {
    this->SetInputData(pd);
    if (scalars)
      {
      this->ColorComponents = scalars->GetNumberOfComponents();
      }
    }

  if (scalars && scalars->GetNumberOfComponents() == 1)
    {
    if (pieceNum == 0)
      {
      this->Pieces[pieceNum]->ScalarScale = 1.0;
      this->Pieces[pieceNum]->ScalarOffset = 0.0;
      }
    else
      {
      vtkDataArray *pda = pd->GetFieldData()->GetArray("PointDataCompressionFactors");
      vtkPointData *pointD = pd->GetPointData();
      int count = 0;
      int indexCount = 0;
      bool done = false;
      while (!done && count < pointD->GetNumberOfArrays())
        {
        vtkDataArray *da = pointD->GetArray(count);
        if (da == scalars)
          {
          done = true;
          }
        else
          {
          indexCount += da->GetNumberOfComponents();
          count++;
          }
        }
	    double offset2, scale2, offset1, scale1;
      pda->GetTuple(indexCount * 2, &offset2);
      pda->GetTuple(indexCount*2+1, &scale2);
      pda = this->Pieces[0]->Input->GetFieldData()->GetArray("PointDataCompressionFactors");
      pda->GetTuple(indexCount*2, &offset1);
      pda->GetTuple(indexCount*2+1, &scale1);
      this->Pieces[pieceNum]->ScalarScale = scale2/scale1;
      this->Pieces[pieceNum]->ScalarOffset = (offset2 - offset1)/scale1;
      }
    }

  // set origin/scale/bounds
  cda->GetTuple(0, this->Pieces[pieceNum]->Origin);
  cda->GetTuple(1, this->Pieces[pieceNum]->Scale);
  this->Pieces[pieceNum]->Bounds[0] = this->Pieces[pieceNum]->Origin[0];
  this->Pieces[pieceNum]->Bounds[2] = this->Pieces[pieceNum]->Origin[1];
  this->Pieces[pieceNum]->Bounds[4] = this->Pieces[pieceNum]->Origin[2];
  this->Pieces[pieceNum]->Bounds[1] = this->Pieces[pieceNum]->Origin[0] +
    this->Pieces[pieceNum]->Scale[0];
  this->Pieces[pieceNum]->Bounds[3] = this->Pieces[pieceNum]->Origin[1] +
    this->Pieces[pieceNum]->Scale[1];
  this->Pieces[pieceNum]->Bounds[5] = this->Pieces[pieceNum]->Origin[2] +
    this->Pieces[pieceNum]->Scale[2];
}

void vtkLODPointCloudMapper::SetNewColorData(const char *name)
{
  for (size_t i = 0; i < this->Pieces.size(); i++)
    {
    this->Pieces[i]->Colors->ReleaseGraphicsResources();
    this->Pieces[i]->Input->GetPointData()->SetActiveScalars(name);
    }

  vtkPointData *pointD = this->Pieces[0]->Input->GetPointData();
  vtkDataArray *scalars = pointD->GetScalars();
  this->ColorComponents = scalars->GetNumberOfComponents();

  if (this->ColorComponents == 1)
    {
    this->Pieces[0]->ScalarScale = 1.0;
    this->Pieces[0]->ScalarOffset = 0.0;

    int count = 0;
    int indexCount = 0;
    bool done = false;
    while (!done && count < pointD->GetNumberOfArrays())
      {
      vtkDataArray *da = pointD->GetArray(count);
      if (da == scalars)
        {
        done = true;
        }
      indexCount += da->GetNumberOfComponents();
      count++;
      }

    double offset1, scale1;
    vtkDataArray *pda = this->Pieces[0]->Input->GetFieldData()->GetArray("PointDataCompressionFactors");
    pda->GetTuple(indexCount * 2, &offset1);
    pda->GetTuple(indexCount * 2 + 1, &scale1);

    for (size_t pieceNum = 1; pieceNum < this->Pieces.size(); pieceNum++)
      {
      vtkPolyData *pd = this->Pieces[pieceNum]->Input;
      vtkDataArray *pda = pd->GetFieldData()->GetArray("PointDataCompressionFactors");
      double offset2, scale2;
      pda->GetTuple(indexCount * 2, &offset2);
      pda->GetTuple(indexCount*2+1, &scale2);
      this->Pieces[pieceNum]->ScalarScale = scale2/scale1;
      this->Pieces[pieceNum]->ScalarOffset = (offset2 - offset1)/scale1;
      }
    }
}

void vtkLODPointCloudMapper::ClearColorData()
{
  this->ColorComponents = 0;
}

float vtkLODPointCloudMapper::ComputeCoverage(
  vtkRenderer *ren,
  vtkActor *act)
{
  vtkLODPointCloudMapperPiece *prop;
  double total_time;
  double *bounds;
  double planes[24], d;
  double coverage;
  int    i, propLoop;

  // We will return the total time of all props. This is used for
  // normalization.
  total_time  = 0;

  // Get the view frustum planes from the active camera
  vtkCamera *cam = ren->GetActiveCamera();
  cam->GetFrustumPlanes(
    ren->GetTiledAspectRatio(), planes );

  // we use the temp matric
  act->GetMatrix(this->TempMatrix4);

  // For each prop, compute coverage
  for ( propLoop = 0; propLoop < this->Pieces.size(); propLoop++ )
    {
    // Get the piece out of the list
    prop = this->Pieces[propLoop];
    prop->ScreenCoverage = 0.0;
    prop->VolumeCoverageRatio = 0.0;

    // Get the bounds of the prop and compute an enclosing sphere
    if (prop->Input && prop->Input->GetNumberOfPoints() > 1)
      {
      bounds = prop->Bounds;

      // We start with a coverage of 1.0 and set it to zero if the prop
      // is culled during the plane tests
      coverage = 1.0;

      // for each plane
      float minDistances[6];
      float limitedMinDistances[6];
      for ( i = 0; i < 6; i++ )
        {
        // Compute how far the closest bound point is from this plane
        bool initialized = false;
        for(int ix = 0; ix<=1; ix++)
          {
          for(int iy = 0; iy<=1; iy++)
            {
            for(int iz = 0; iz<=1; iz++)
              {
              double x[4]={bounds[ix],bounds[2+iy],bounds[4+iz],1.0};
              this->TempMatrix4->MultiplyPoint(x,x);
              d =
                planes[i*4 + 0] * x[0] +
                planes[i*4 + 1] * x[1] +
                planes[i*4 + 2] * x[2] +
                planes[i*4 + 3];
              if (!initialized || d < minDistances[i])
                {
                minDistances[i] = d;
                initialized = true;
                }
              }
            }
          }
        limitedMinDistances[i] = minDistances[i];
        if (limitedMinDistances[i] < 0.0)
          {
          limitedMinDistances[i] = 0.0;
          }
        }

      // screen coverage is based on the first four planes
      double *cRange = cam->GetClippingRange();

      // coverage is far - near of bounding box divided by
      // far - near of frustum
      coverage = (cRange[1] - limitedMinDistances[5]  - cRange[0] - limitedMinDistances[4])
        /(cRange[1] - cRange[0]);

      // if we are not fully clipped yet compute horizontal and vertical
      if (coverage > 0.0)
        {
        double farDist = cRange[1] - limitedMinDistances[5];
        double *pos =  cam->GetPosition();
        double *dop = cam->GetDirectionOfProjection();
        double distPoint[3];
        for (int i = 0; i <3; i ++)
          {
          distPoint[i] = pos[i] + dop[i]*farDist;
          }
        double hDist =
          planes[0] * distPoint[0] +
          planes[1] * distPoint[1] +
          planes[2] * distPoint[2] +
          planes[3] +
          planes[4] * distPoint[0] +
          planes[5] * distPoint[1] +
          planes[6] * distPoint[2] +
          planes[7];
        double vDist =
          planes[8] * distPoint[0] +
          planes[9] * distPoint[1] +
          planes[10] * distPoint[2] +
          planes[11] +
          planes[12] * distPoint[0] +
          planes[13] * distPoint[1] +
          planes[14] * distPoint[2] +
          planes[15];
        double screenCoverage =
          (hDist - limitedMinDistances[0] - limitedMinDistances[1])/hDist;
        if (screenCoverage < 0.0)
          {
          screenCoverage = 0.0;
          }
        screenCoverage = screenCoverage*
          (vDist - limitedMinDistances[2] - limitedMinDistances[3])/vDist;
        if (screenCoverage < 0.0)
          {
          screenCoverage = 0.0;
          }
        prop->ScreenCoverage = screenCoverage;
        // items closer to the camera are more valuable
        prop->VolumeCoverageRatio = 1.0 - limitedMinDistances[4]/cRange[1];
        // coverage *= screenCoverage;
        // float coverageRatio = (cRange[1] - cRange[0] - minDistances[5] - minDistances[4])
        //   *(hDist - minDistances[1] - minDistances[0])*(vDist - minDistances[3] - minDistances[2]);
        // prop->VolumeCoverageRatio = (coverage*(cRange[1] - cRange[0])*hDist*vDist) / coverageRatio;
        }
      }
    }

  float totalCM = 0.0;
  for (size_t i = 0; i < this->Pieces.size(); i++ )
    {
    this->Pieces[i]->CoverageMetric =
      this->Pieces[i]->VolumeCoverageRatio *
      this->Pieces[i]->ScreenCoverage;
    totalCM +=  this->Pieces[i]->CoverageMetric;
    }

  // figure out how much time we have to render and
  // compute how many points we get to draw for the
  // pieces that have non zero coverage
  float myTime = act->GetAllocatedRenderTime();
  float pointLimit = this->LastPointRate*myTime;

  // now allocate those points across the pieces intelligently
  // this is a little tricky as the max percentage is 100% so
  // a simple weighting does not work.
  float cmToPoints = pointLimit/totalCM;
  float lastCMToPoints;
  int count = 0;
  do
    {
    lastCMToPoints = cmToPoints;
    float totRendered = 0;
    float opportunityPoints = 0;
    float opportunityPoints2 = 0;
    for (int i = 0; i < this->Pieces.size(); i++ )
      {
      if (this->Pieces[i]->CoverageMetric > 0.0)
        {
        // compute the target number of points to render,
        // if we have extra capacity then pass it on to the next piece
        float targetPoints = this->Pieces[i]->CoverageMetric*cmToPoints;
        if (targetPoints > this->Pieces[i]->VertexCount*0.1)
          {
          if (targetPoints >= this->Pieces[i]->VertexCount)
            {
            totRendered += this->Pieces[i]->VertexCount;
            }
          else
            {
            totRendered += targetPoints;
            opportunityPoints += (this->Pieces[i]->VertexCount - targetPoints);
            opportunityPoints2 += targetPoints;
            }
          }
        }
      }
    // if we have extra points adjust the ratio to try to consume them
    if (totRendered < pointLimit && opportunityPoints2)
      {
      float extraPoints = pointLimit - totRendered;
      cmToPoints = cmToPoints*(0.5 + 0.5*(1.0 + extraPoints/opportunityPoints2));
      }
    count++;
    }
  while (fabs(lastCMToPoints - cmToPoints)/(lastCMToPoints+cmToPoints) > 0.05 && count < 5);

  // finally return the ratio we determined
  return cmToPoints;
}

void vtkLODPointCloudMapper::UpdateShaders(vtkOpenGLRenderWindow *renWin)
{
  if (this->LastColorComponents != this->ColorComponents)
    {
    if (this->ColorComponents == 1)
      {
      this->ShaderProgram =
        renWin->GetShaderCache()->ReadyShaderProgram(
        "//VTK::System::Dec\n"
        "uniform mat4 MCDCMatrix;\n"
        "uniform float scalarScale;\n"
        "uniform float scalarOffset;\n"
        "attribute vec4 vertexMC;\n"
        "attribute float scalarColor;\n"
        "varying float vertexScalarVSOutput;\n"
        "void main() {\n"
        "  vertexScalarVSOutput = scalarColor*scalarScale+scalarOffset;\n"
        "  gl_Position = MCDCMatrix * vertexMC;\n"
        "}\n",
        "//VTK::System::Dec\n"
        "varying float vertexScalarVSOutput;\n"
        "//VTK::Output::Dec\n"
        "void main(void) {\n"
        "  vec3 color =  mix(vec3(0.230, 0.299, 0.754), vec3(0.865, 0.865, 0.865), clamp(vertexScalarVSOutput+0.5,0.0,1.0));\n"
        "  color = mix(color,vec3(0.706, 0.016, 0.150),clamp(vertexScalarVSOutput-0.5,0.0,1.0));\n"
        "  gl_FragData[0] = vec4(color,1.0);\n"
        "}\n",
        "");
      }
    else if (this->ColorComponents == 3)
      {
      this->ShaderProgram =
        renWin->GetShaderCache()->ReadyShaderProgram(
        "//VTK::System::Dec\n"
        "uniform mat4 MCDCMatrix;\n"
        "attribute vec4 vertexMC;\n"
        "attribute vec3 scalarColor;\n"
        "varying vec3 vertexColorVSOutput;\n"
        "void main() {\n"
        "  vertexColorVSOutput =  scalarColor;\n"
        "  gl_Position = MCDCMatrix * vertexMC;\n"
        "}\n",
        "//VTK::System::Dec\n"
        "varying vec3 vertexColorVSOutput;\n"
        "//VTK::Output::Dec\n"
        "void main(void) {\n"
        "  gl_FragData[0] = vec4(vertexColorVSOutput,1.0);\n"
        "}\n",
        "");
      }
    else if (this->ColorComponents == 4)
      {
      this->ShaderProgram =
        renWin->GetShaderCache()->ReadyShaderProgram(
          "//VTK::System::Dec\n"
          "uniform mat4 MCDCMatrix;\n"
          "attribute vec4 vertexMC;\n"
          "attribute vec4 scalarColor;\n"
          "varying vec4 vertexColorVSOutput;\n"
          "void main() {\n"
          "  vertexColorVSOutput =  scalarColor;\n"
          "  gl_Position = MCDCMatrix * vertexMC;\n"
          "}\n",
          "//VTK::System::Dec\n"
          "varying vec4 vertexColorVSOutput;\n"
          "//VTK::Output::Dec\n"
          "void main(void) {\n"
          "  gl_FragData[0] = vertexColorVSOutput;\n"
          "}\n",
          "");
      }
    else
      {
      this->ShaderProgram =
        renWin->GetShaderCache()->ReadyShaderProgram(
          "//VTK::System::Dec\n"
          "uniform mat4 MCDCMatrix;\n"
          "attribute vec4 vertexMC;\n"
          "void main() {\n"
          "  gl_Position = MCDCMatrix * vertexMC;\n"
          "}\n",
          "//VTK::System::Dec\n"
          "//VTK::Output::Dec\n"
          "uniform float nearC;\n"
          "uniform float farC;\n"
          "void main(void) {\n"
          "  float fbdepth = 2.0 * gl_FragCoord.z - 1.0;\n"
          "  fbdepth = 2.0*nearC*farC/(farC + nearC - fbdepth*(farC - nearC));\n"
          "  fbdepth = (fbdepth - nearC)/(farC - nearC);\n"
          "  gl_FragData[0] = vec4(1.0 - fbdepth,1.0 - fbdepth,0.5,1.0);\n"
          "}\n",
          "");
      }
    this->LastColorComponents = this->ColorComponents;
    this->ShaderBuildTime.Modified();
    }
  else
    {
    renWin->GetShaderCache()->ReadyShaderProgram(this->ShaderProgram);
    }
}

void vtkLODPointCloudMapper::BuildVectorFromInput()
{
  vtkMultiBlockDataSet *input =
    vtkMultiBlockDataSet::SafeDownCast(this->GetInputDataObject(0, 0));
  if (input)
    {
    vtkDataObjectTreeIterator* iter = input->NewTreeIterator();
    iter->TraverseSubTreeOff();
    iter->VisitOnlyLeavesOff();

    int blockIdx = 0;
    for (iter->InitTraversal();
         !iter->IsDoneWithTraversal() && !this->AbortExecute;
         iter->GoToNextItem(), blockIdx++)
      {
      vtkDataObject* dObj = iter->GetCurrentDataObject();
      if (dObj)
        {
        // do we need to compress our input?
        vtkDataArray *cda = dObj->GetFieldData()->GetArray("PointCompressionFactors");
        if (cda)
          {
          this->SetDataForPiece(blockIdx, vtkPolyData::SafeDownCast(dObj));
          }
        else
          {
          vtkDataObject* block = vtkMultiBlockToCompressedRange::Compress(dObj);
          if (block)
            {
            this->SetDataForPiece(blockIdx, vtkPolyData::SafeDownCast(block));
            block->Delete();
            }
          }
        }
      }

    iter->Delete();
    }
  this->VectorBuildTime.Modified();
}

//----------------------------------------------------------------------------
void vtkLODPointCloudMapper::Render(vtkRenderer *ren, vtkActor *act)
{
  // Make sure that we have been properly initialized.
  if (ren->GetRenderWindow()->CheckAbortStatus())
    {
    return;
    }

  if (this->GetNumberOfInputConnections(0))
    {
    vtkInformation *inInfo = this->GetInputInformation();
    this->GetInputAlgorithm()->UpdateInformation();
    vtkCompositeDataSet *input = vtkCompositeDataSet::SafeDownCast(
      this->GetInputDataObject(0, 0));

    if (input != NULL)
      {
      // stream in future?
      inInfo->Set(
        vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(), 0);
      inInfo->Set(
        vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(), 1);
      inInfo->Set(
        vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS(),
        this->GhostLevel);

      this->InvokeEvent(vtkCommand::StartEvent,NULL);
      if (!this->Static)
        {
        this->GetInputAlgorithm()->Update();
        }
      this->InvokeEvent(vtkCommand::EndEvent,NULL);
      }

    // if the input is more recent than our processed vector
    // then rebuild our vector
    if (this->Pieces.size() < 1 ||
        (input && input->GetMTime() > this->VectorBuildTime))
      {
      this->BuildVectorFromInput();
      }
    }

  // compile and bind the shader
  vtkOpenGLRenderWindow *renWin =
    vtkOpenGLRenderWindow::SafeDownCast(ren->GetRenderWindow());
  this->UpdateShaders(renWin);

  // update the global parameters for the shader
  vtkOpenGLCamera *cam = (vtkOpenGLCamera *)(ren->GetActiveCamera());

  vtkMatrix4x4 *wcdc;
  vtkMatrix4x4 *wcvc;
  vtkMatrix3x3 *norms;
  vtkMatrix4x4 *vcdc;
  cam->GetKeyMatrices(ren,wcvc,norms,vcdc,wcdc);

  vtkMatrix4x4* mcwc;
  vtkMatrix3x3* anorms;
  ((vtkOpenGLActor*)act)->GetKeyMatrices(mcwc,anorms);

  vtkMatrix4x4::Multiply4x4(mcwc, wcdc, this->MCDCMatrix);

  // if (this->ShaderProgram->IsUniformUsed("cameraParallel"))
  //   {
  //   this->ShaderProgram->SetUniformi("cameraParallel",
  //     cam->GetParallelProjection());
  //   }

  if (!this->ColorComponents)
    {
    this->ShaderProgram->SetUniformf("nearC",cam->GetClippingRange()[0]);
    this->ShaderProgram->SetUniformf("farC",cam->GetClippingRange()[1]);
    }

  // to LOD well we need to know how much time we spend rendering
#if GL_ES_VERSION_2_0 != 1 && GL_ES_VERSION_3_0 != 1
  bool createdTimer = false;
  if (this->TimerQuery == 0)
    {
    glGenQueries(1, static_cast<GLuint*>(&this->TimerQuery));
    glBeginQuery(GL_TIME_ELAPSED, static_cast<GLuint>(this->TimerQuery));
    createdTimer = true;
    }
  else
    {
    GLint timerAvailable = 0;
    glGetQueryObjectiv(static_cast<GLuint>(this->TimerQuery),
      GL_QUERY_RESULT_AVAILABLE, &timerAvailable);

    if (timerAvailable)
      {
      // See how much time the rendering of the mapper took
      // in nanoseconds during the previous frame
      GLuint timeElapsed = 0;
      glGetQueryObjectuiv(static_cast<GLuint>(this->TimerQuery),
        GL_QUERY_RESULT, &timeElapsed);
      // Set the rendering time for this frame with the previous one
      this->TimeToDraw = timeElapsed / 1.0e9;
      if (this->LastTimedPoints > 0 && this->TimeToDraw > 0.0)
        {
        this->LastPointRate =
          this->LastPointRate*0.6 +
          0.4*this->LastTimedPoints/this->TimeToDraw;
        }
      glBeginQuery(GL_TIME_ELAPSED, static_cast<GLuint>(this->TimerQuery));
      createdTimer = true;
      }
    }
#endif

  //
  // Compute and allocate the coverage
  // this funciton returns a value indicating
  // how many points shoudl be rendered per unit
  // of the coverage metric
  float cmToPoints = this->ComputeCoverage(ren, act);

  //
  //  Now draw the points using the coverage metric
  // and ratio computed by ComputeCoverage
  int pointsRendered = 0.0;
  for (int i = 0; i < this->Pieces.size(); i++ )
    {
    if (this->Pieces[i]->CoverageMetric > 0.0)
      {
      // compute the target number of points to render,
      // if we have extra capacity then pass it on to the next piece
      float targetPoints = this->Pieces[i]->CoverageMetric*cmToPoints;
      if (targetPoints < 100)
        {
        targetPoints = 100;
        }
      if (targetPoints > this->Pieces[i]->VertexCount)
        {
        targetPoints = this->Pieces[i]->VertexCount;
        }
      // only render if it will result in at least a reasonable number of points
      if (!i || targetPoints > this->Pieces[i]->VertexCount*0.1)
        {
        this->RenderPointCloudPiece(ren, act,
          static_cast<vtkLODPointCloudMapperPiece *>(this->Pieces[i]),
          static_cast<size_t>(targetPoints),
          this->MCDCMatrix);
        pointsRendered += static_cast<int>(targetPoints);
        }
      }
    }

#if GL_ES_VERSION_2_0 != 1 && GL_ES_VERSION_3_0 != 1
  if (createdTimer)
    {
    glEndQuery(GL_TIME_ELAPSED);
    this->LastTimedPoints = pointsRendered;
    }
#endif
}

//----------------------------------------------------------------------------
void vtkLODPointCloudMapper::RenderPointCloudPiece(
  vtkRenderer * /*ren*/,
  vtkActor * /*act*/,
  vtkLODPointCloudMapperPiece *piece,
  size_t targetPoints,
  vtkMatrix4x4 *wcdc)
{
  if (!piece->VertexCount)
    {
    return;
    }

  // clear VAO if needed
  if (piece->VAOBuildTime < this->ShaderBuildTime)
    {
    piece->VAO->ReleaseGraphicsResources();
    }

  // build the VBO if needed
  piece->VAO->Bind();
  if (piece->PointsVBOBuildTime < piece->Input->GetPoints()->GetMTime())
    {
    piece->Points->Upload(
      static_cast<unsigned short *>(piece->Input->GetPoints()->GetVoidPointer(0)),
      piece->VertexCount*3,
      vtkOpenGLBufferObject::ArrayBuffer);
    piece->PointsVBOBuildTime.Modified();
    }

  vtkDataArray *scalars = piece->Input->GetPointData()->GetScalars();
  if (scalars &&
      piece->ColorsVBOBuildTime < piece->Input->GetPointData()->GetMTime())
    {
    if (scalars->GetDataType() == VTK_UNSIGNED_CHAR)
      {
      piece->Colors->Upload(
        static_cast<unsigned char *>(scalars->GetVoidPointer(0)),
        piece->Input->GetNumberOfPoints()*this->ColorComponents,
        vtkOpenGLBufferObject::ArrayBuffer);
      }
    else
      {
      piece->Colors->Upload(
        static_cast<unsigned short *>(scalars->GetVoidPointer(0)),
        piece->Input->GetNumberOfPoints()*this->ColorComponents,
        vtkOpenGLBufferObject::ArrayBuffer);
      }
    piece->ColorsVBOBuildTime.Modified();
    }

  // clear VAO if needed
  if (piece->VAOBuildTime < this->ShaderBuildTime ||
      piece->VAOBuildTime < piece->ColorsVBOBuildTime ||
      piece->VAOBuildTime < piece->PointsVBOBuildTime)
    {
    piece->Points->Bind();
    if (!piece->VAO->AddAttributeArray(this->ShaderProgram, piece->Points,
                                    "vertexMC", 0,
                                     6, VTK_UNSIGNED_SHORT, 3, false))
      {
      vtkErrorMacro(<< "Error setting 'vertexMC' in shader VAO.");
      }
    piece->Points->Release();
    if (this->ColorComponents > 0)
      {
      piece->Colors->Bind();
      if (scalars->GetDataType() == VTK_UNSIGNED_CHAR)
        {
        if (!piece->VAO->AddAttributeArray(this->ShaderProgram, piece->Colors,
            "scalarColor", 0,
            this->ColorComponents, VTK_UNSIGNED_CHAR,
            this->ColorComponents, true))
          {
          vtkErrorMacro(<< "Error setting 'scalarColor' in shader VAO.");
          }
        }
      else
        {
        if (!piece->VAO->AddAttributeArray(this->ShaderProgram, piece->Colors,
            "scalarColor", 0,
            this->ColorComponents*2, VTK_UNSIGNED_SHORT,
            this->ColorComponents, true))
          {
          vtkErrorMacro(<< "Error setting 'scalarColor' in shader VAO.");
          }
        }
      piece->Colors->Release();
      }
    else
      {
      piece->VAO->RemoveAttributeArray("scalarColor");
      }
    piece->VAOBuildTime.Modified();
    }

  // compute and send matrix uniform
  this->TempTransform->Identity();
  this->TempTransform->Translate(piece->Origin);
  this->TempTransform->Scale(
    piece->Scale[0] / 65535.0,
    piece->Scale[1] / 65535.0,
    piece->Scale[2] / 65535.0);
  this->TempTransform->GetTranspose(this->TempMatrix4);
  vtkMatrix4x4::Multiply4x4(this->TempMatrix4, wcdc, this->TempMatrix4);
  this->ShaderProgram->SetUniformMatrix("MCDCMatrix", this->TempMatrix4);
  if (scalars && scalars->GetNumberOfComponents() == 1)
    {
    this->ShaderProgram->SetUniformf("scalarScale", piece->ScalarScale);
    this->ShaderProgram->SetUniformf("scalarOffset", piece->ScalarOffset);
    }
  // draw polygons
  glDrawArrays(GL_POINTS, 0,
      static_cast<GLuint>(targetPoints));
  piece->VAO->Release();
}

void vtkLODPointCloudMapper::ReleaseGraphicsResources(vtkWindow *win)
{
  for (int i = 0; i < this->Pieces.size(); i++)
    {
    if (this->Pieces[i])
      {
      this->Pieces[i]->ReleaseGraphicsResources(win);
      }
    }
}

//-----------------------------------------------------------------------------
void vtkLODPointCloudMapper::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
